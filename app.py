from flask import Flask, request
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import jsonify
from marshmallow import Schema, fields, ValidationError, post_load, validate
import os
from os.path import join, dirname
from dotenv import load_dotenv
from flask_bcrypt import Bcrypt

#### dotenv config #########
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)


# flask app object
app = Flask(__name__)

#########       Models         #######################
# app config
app.config['SQLALCHEMY_DATABASE_URI']=f"postgresql://{os.environ.get('USER_NAME')}:{os.environ.get('USER_PASSWORD')}@{os.environ.get('HOST')}:{os.environ.get('DATABASE_PORT')}/{os.environ.get('DATABASE_NAME')}"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
db = SQLAlchemy(app)
Migrate(app, db)

class User(db.Model):

    __tablename__ = 'user'

    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), index=True)
    password = db.Column(db.String(128))
    role = db.Column(db.String(50))
    address = db.Column(db.String(50))
    email = db.Column(db.String(50))

    def __init__(self, username, password, role, address, email):
        self.username = username
        self.password = password
        self.role = role
        self.address = address
        self.email = email

    def __repr__(self):
        return f"UserName: {self.username}, Role: {self.role}"

###############     Marshmallow     ##############################

class UserSchema(Schema):
    username = fields.Str(required=True, validate=validate.Length(min=1))
    password = fields.Str(required=True)
    role = fields.Str(validate=validate.OneOf(["admin", "user", "delivery"]))
    address = fields.Str()
    email = fields.Email(required=True)

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)

user_schema = UserSchema()
users_schema = UserSchema(many=True)

################# Resource #########################
class UserResource(Resource):

    def get(self):
        users = User.query.all()
        return users_schema.dump(users)

    def post(self):
        bcrypt = Bcrypt()
        sec_pass = 'secret_password'
        data = {"username": request.json['username'], "password": bcrypt.generate_password_hash(request.json['password']), "role": request.json['role'], "address": request.json['address'], "email": request.json['email']}
        try:    
            user_data = user_schema.load(data)
        except ValidationError as err:
            return {'err.messages': err.messages, 'err.valid_data': err.valid_data}

        db.session.add(user_data)
        db.session.commit()
        return user_schema.dump(user_data)

class UserIdResource(Resource):

    def get(self, userid):
        user = User.query.get(userid)
        return user_schema.dump(user)

    def delete(self, userid):
        user = User.query.get(userid)
        db.session.delete(user)
        db.session.commit()
        return user_schema.dump(user)

################# rest ###########################
api = Api(app)

api.add_resource(UserResource, '/users')
api.add_resource(UserIdResource, '/users/<userid>')

if __name__ == '__main__':
    app.run(debug=True)
